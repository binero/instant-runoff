#[derive(Debug)]
pub struct Vote {
    // from least preferred to most preferred
    choices: Vec<String>,
}

impl Vote {
    pub fn new(mut candidates: Vec<String>) -> Vote {
        candidates.reverse();
        Vote {
            choices: candidates,
        }
    }

    pub fn get_next_choice(&mut self) -> Option<String> {
        self.choices.pop()
    }
}
