use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::PathBuf,
};
use vote::Vote;

use clap::Parser;

mod instant_runoff_counter;
mod vote;

#[derive(Parser, Debug)]
struct CliOpts {
    filename: PathBuf,
}

fn main() -> Result<()> {
    let args = CliOpts::parse();

    let file = File::open(&args.filename)?;
    let reader = BufReader::new(file);

    /*
    let votes = reader
        .lines()
        .map(|lineres| {
            lineres.map(|line| {
                let choices = line
                    .split(';')
                    .filter(|choice| !choice.is_empty())
                    .map(|vote| vote.to_string())
                    .collect::<Vec<_>>();
                Vote::new(choices)
            })
        })
        .collect::<Result<Vec<_>, _>>()?;
     */

    let votes = reader
        .lines()
        .map(|lineres| {
            lineres.map(|line| {
                let mut choices = line.split(';').enumerate().collect::<Vec<_>>();
                choices.sort_unstable_by_key(|&(_candidate, rank)| rank);
                Vote::new(
                    choices
                        .into_iter()
                        .map(|(candidate, _)| format!("{candidate}"))
                        .collect(),
                )
            })
        })
        .collect::<Result<Vec<_>, _>>()?;

    let mut counter = instant_runoff_counter::InstantRunoffCounter::new(votes);

    println!("{counter:?}");

    {
        let mut round = 0;
        while counter.scores().count() > 1 {
            println!("Round {}:", round);
            let scores = counter.scores_sorted();
            for (candidate, score) in scores {
                println!(" -> {}: {}", candidate, score);
            }
            println!();
            counter.run_round();
            round += 1;
        }
    }

    println!("Winner:");
    println!(" -> {}", counter.winner().expect("no winner found"));

    Ok(())
}
