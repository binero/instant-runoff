use std::collections::HashMap;

use rand::{prelude::SliceRandom, thread_rng};

use crate::Vote;

#[derive(Debug)]
pub struct InstantRunoffCounter {
    candidates: HashMap<String, Vec<Vote>>,
}

impl InstantRunoffCounter {
    pub fn new(votes: Vec<Vote>) -> Self {
        let candidates = HashMap::new();

        let mut counter = InstantRunoffCounter { candidates };

        for vote in votes {
            counter.add_vote(vote);
        }

        counter
    }

    fn add_vote(&mut self, mut vote: Vote) {
        if let Some(choice) = vote.get_next_choice() {
            let votes = self.candidates.entry(choice).or_insert(vec![]);
            votes.push(vote);
        }
    }

    fn add_existing(&mut self, mut vote_to_add: Vote) {
        while let Some(choice) = vote_to_add.get_next_choice() {
            let votes = self.candidates.get_mut(&choice);

            if let Some(votes) = votes {
                votes.push(vote_to_add);
                break;
            }
        }
    }

    pub fn scores(&self) -> impl Iterator<Item = (&str, usize)> {
        self.candidates
            .iter()
            .map(|(candidate, votes)| (candidate.as_str(), votes.len()))
    }

    pub fn scores_sorted(&self) -> Vec<(&str, usize)> {
        let mut scores: Vec<_> = self.scores().collect();
        scores.shuffle(&mut thread_rng());
        scores.sort_unstable_by_key(|&(_, score)| score);
        scores.reverse();
        scores
    }

    fn lowest_scoring_candidate(&self) -> &str {
        self.scores()
            .min_by_key(|&(_, score)| score)
            .map(|(candidate, _)| candidate)
            .expect("expected a candidate to be left")
    }

    pub fn run_round(&mut self) {
        let lowest_scoring_candidate = self.lowest_scoring_candidate().to_string();

        let votes_to_transfer = self.candidates.remove(&lowest_scoring_candidate).unwrap();

        for vote in votes_to_transfer.into_iter() {
            self.add_existing(vote);
        }
    }

    pub fn winner(&self) -> Option<&str> {
        self.scores()
            .max_by_key(|&(_, score)| score)
            .map(|(candidate, _)| candidate)
    }
}
